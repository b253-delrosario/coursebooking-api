const Course = require("../models/Course");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/
module.exports.addCourse = (reqBody) => {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		});

		return newCourse.save().then(course => true)
			.catch(err => false);
};

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => result).catch(err => err);
};

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => result).catch(err => err);
};

module.exports.getSpecificCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => result).catch(err => err);
};

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description:reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams, updatedCourse).then(course => true).catch(err => err);
};


module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse = {
		isActive: reqBody.isActive
	};
	return Course.findByIdAndUpdate(reqParams, archivedCourse).then(course => true).catch(err => err);

};
